/** @type {import('next').NextConfig} */
const nextConfig = {
    output: 'standalone',
    basePath: '/gitlab-test',
    // assetPrefix: '/gitlab-test/'
}

module.exports = nextConfig
